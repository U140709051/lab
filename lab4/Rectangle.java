

public class Rectangle {

	int sideA;
	int sideB;
	
	public Rectangle(int a , int b){
		
		System.out.println("creating rectangle");
		sideA = a ;
		sideB = b ;
	}
	
	public int area(){
	int result = sideA*sideB;
	return result;
	
	}
	
	public int perimeter(){
		
		return 2 * (sideA + sideB);
		
		
		
	}
}
