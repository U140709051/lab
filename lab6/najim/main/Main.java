package najim.main;
import  najim.shapes.*;
import java.util.ArrayList;
public class Main {

	public static void main(String[] args) {
		Circle circle = new Circle(5);
		System.out.println(circle.area());
		
		Rectangle rect = new Rectangle(5,6);
		System.out.println(rect.area());
		
		ArrayList<Circle> circles = new ArrayList();
		circles.add(circle);
		circles.add(new Circle(6));
		circles.add(new Circle(7));
		
		System.out.println();
		
		Drawing drawing = new Drawing();
		for (int i= 0 ; i < circles.size(); i++ ){
			Circle circ= circles.get(i);
			drawing.addCircle(circ);
			
		}
		drawing.printAreas();
	
	}
	

}