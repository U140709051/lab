package najim.shapes3d;
import najim.shapes.Circle;
public class Cylinder extends Circle{ 

	private int height;
	
public cylinder (int radius, int height){
	super(radius);
	this.height = height;
	
	
}
	public double area(){
		return 2*super.area() + height *2 * Math.PI* getRadius;
	}
	
	public double volume(){
		return super.area() * height ;
	}
	public String toString(){
		return "radius: " + getRadius()+ ", height: " + height;
	}
}
