package najim.shapes;

public class Circle {
	public int radius;
	
	public Circle(int radius){
		this.radius = radius;
	}
	public double area(){
		
		return 3.14*radius*radius;
	}
	public int getRadius(){
		return radius;
	}
}
